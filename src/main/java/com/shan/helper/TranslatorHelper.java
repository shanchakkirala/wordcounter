package com.shan.helper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.shan.translator.TranslatorFactory;

@Component
public class TranslatorHelper {
	
	@Autowired
	TranslatorFactory factory;
	
	public boolean getTranslatedWord(String wordToCheck, String wordInList) {
		return factory.getTranslatedWord(wordToCheck,wordInList);
	}

}
