package com.shan.app;

import java.util.TimeZone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


@Configuration
@ComponentScan({ "com.shan.helper", 
	"com.shan.translator", 
	"com.shan.validation",
	"com.shan.wordcounter"})
@SpringBootApplication
public class WordCounterApp  {
	 public static void main(String[] args) {
	        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
	        SpringApplication.run(WordCounterApp.class, args);
	    }

}
