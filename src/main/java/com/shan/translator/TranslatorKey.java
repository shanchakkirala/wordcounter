package com.shan.translator;

public class TranslatorKey {
	
	private String key;
	
	public TranslatorKey(String key) {
		this.key= key.toLowerCase();
	}
	
	public String getKey() {
		return key;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((key == null) ? 0 : key.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TranslatorKey other = (TranslatorKey) obj;
			if (key == null) {
				if (other.key != null)
					return false;
			} else if (!key.equalsIgnoreCase(other.key))
				return false;
		return true;
	}
	
	
}
