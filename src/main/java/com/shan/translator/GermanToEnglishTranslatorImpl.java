package com.shan.translator;

import java.util.HashMap;
import java.util.Map;

public class GermanToEnglishTranslatorImpl implements Translator{
	
final Map<TranslatorKey,String> translatedWordsMap = new HashMap<>();
	
	@Override
	public String translate(String word) {
		return translatedWordsMap.get(new TranslatorKey(word));
	}
	
	public Map<TranslatorKey,String> getTranslatedMap() {
		return translatedWordsMap;
	}
	
	public void populateMap() {
		translatedWordsMap.put(new TranslatorKey("blume"), "flower");
		translatedWordsMap.put(new TranslatorKey("kaffee"), "coffee");
	}

}
