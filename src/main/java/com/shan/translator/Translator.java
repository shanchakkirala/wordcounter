package com.shan.translator;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;


@Service
public interface Translator {
	
	public static final Map<String,Translator> translatorsMap = new HashMap<>();
		
	public String translate(String word);
	
	public void populateMap();

	Map<TranslatorKey, String> getTranslatedMap();

}
