package com.shan.translator;

import java.util.HashMap;
import java.util.Map;

public class EnglishToGermanTranslatorImpl implements Translator {
	
final Map<TranslatorKey,String> translatedWordsMap = new HashMap<>();
	
	@Override
	public String translate(String word) {
		TranslatorKey key = new TranslatorKey(word);
		return translatedWordsMap.get(key);
	}
	
	@Override
	public Map<TranslatorKey,String> getTranslatedMap() {
		return translatedWordsMap;
	}
	
	@Override
	public void populateMap() {
		translatedWordsMap.put(new TranslatorKey("flower"), "blume");
		translatedWordsMap.put(new TranslatorKey("coffee"), "kaffee");
		
	}

}
