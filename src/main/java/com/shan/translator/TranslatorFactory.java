package com.shan.translator;

import java.util.Map;

import org.springframework.stereotype.Component;

@Component
public class TranslatorFactory {

//	public static final Map<String,String> translatedWordsMap = new HashMap<>();
	
		
	public static final String LANGUAGE_HINDI = "HindiToEnglish";
	public static final String LANGUAGE_FRENCH = "FrenchToEnglish";
	public static final String LANGUAGE_GERMAN = "GermanToEnglish";

	private static final String LANGUAGE_ENGLISH_GERMAN = "EnglishToGerman";
	private static final String LANGUAGE_ENGLISH_FRENCH = "EnglishToFrench"; 
	private static final String LANGUAGE_ENGLISH_HINDI = "EnglishToHindi";
	
	public TranslatorFactory() {
		populateTranslatorsMap();
		populateTranslatedWordsMap();
	}
	
	
	public Translator getTranslator(String language) {
		return Translator.translatorsMap.get(language);
	}
	
	public boolean getTranslatedWord(String wordToCheck, String wordInList) {
		for(Translator translator :getAllTranslators().values()) {
//			for(String key : translator.getTranslatedMap().keySet()) {
				String value = translator.getTranslatedMap().get(new TranslatorKey(wordInList));
				if(value != null && value.equalsIgnoreCase(wordToCheck))
					return true;
//			}
		}
		
		return false;
	}
	
	public Map<String,Translator> getAllTranslators() {
		return Translator.translatorsMap;
	}
	
	public void populateTranslatorsMap() {
		Translator.translatorsMap.put(LANGUAGE_FRENCH, new SpanishToEnglishTranslatorImpl());
		Translator.translatorsMap.put(LANGUAGE_HINDI, new HindiToEnglishTranslatorImpl());
		Translator.translatorsMap.put(LANGUAGE_GERMAN, new GermanToEnglishTranslatorImpl());
		Translator.translatorsMap.put(LANGUAGE_ENGLISH_GERMAN, new EnglishToGermanTranslatorImpl());
		Translator.translatorsMap.put(LANGUAGE_ENGLISH_FRENCH, new EnglishToSpanishTranslatorImpl());
		Translator.translatorsMap.put(LANGUAGE_ENGLISH_HINDI, new EnglishToHindiTranslatorImpl());
		
	}
		
	public void populateTranslatedWordsMap() {
		for(String language : getAllTranslators().keySet()) {
			Translator translator = getAllTranslators().get(language);
			translator.populateMap();			
		}
//		Map<String,String> map = Translator.translatedWordsMap;
//		for(String key : map.keySet()) {
//			translatedWordsMap.put(key,map.get(key));
//		}
	}
}
