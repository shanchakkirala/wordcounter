package com.shan.translator;

import java.util.HashMap;
import java.util.Map;

public class HindiToEnglishTranslatorImpl implements Translator {

final Map<TranslatorKey,String> translatedWordsMap = new HashMap<>();
	
	@Override
	public String translate(String word) {
		return translatedWordsMap.get(new TranslatorKey(word));
	}
	
	@Override
	public Map<TranslatorKey,String> getTranslatedMap() {
		return translatedWordsMap;
	}
	
	@Override
	public void populateMap() {
		translatedWordsMap.put(new TranslatorKey("patthar"), "stone");
		translatedWordsMap.put(new TranslatorKey("phool"), "flower");

	}

}
