package com.shan.validation;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class ValidationHelper {
	
	public static final String REGEX_ALPHABETS ="^[a-zA-Z]*$";
	public static final String NULL_ERROR = "Word cannot be blank. Please add a word";
	public static final String REGEX_FAILED = "Word should only have alphabets. Please add an alphabetical word";
	
	public List<String> validate(String word) {
		List<String> errors = new ArrayList<>();
 		if(word != null && word.length()>0) {
			if(!word.matches(REGEX_ALPHABETS)) {
				errors.add(REGEX_FAILED);
			} 
		} else {
			errors.add(NULL_ERROR);
		}		
		return errors;
	}

}
