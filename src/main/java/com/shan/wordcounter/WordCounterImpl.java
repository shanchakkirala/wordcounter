package com.shan.wordcounter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.shan.helper.TranslatorHelper;
import com.shan.validation.ValidationHelper;

@Component
public class WordCounterImpl implements WordCounter {
	
	@Autowired
	ValidationHelper validationHelper;
	
	@Autowired
	TranslatorHelper helper;
	
	final List<String> wordCounterList = new ArrayList<>(); 

	public int getWordCounterWordCount(String word) {
		int counter = 0;
		for(String eachWord: wordCounterList) {
			if(eachWord.equalsIgnoreCase(word) || helper.getTranslatedWord(word, eachWord)) {
				counter++;
			}
		}
		return counter;		
	}

	public void addWordToWordCounterList(String word) {
		List<String> errors = validationHelper.validate(word);
		if(errors.isEmpty()) {
			wordCounterList.add(word.trim());
		} 		
	}
}
