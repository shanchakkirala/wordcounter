package com.shan.wordcounter;

import org.springframework.stereotype.Service;

@Service
public interface WordCounter {
	
	public int getWordCounterWordCount(String word);

	public void addWordToWordCounterList(String string);	
}
