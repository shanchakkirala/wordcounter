package com.shan.wordcounter;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.shan.config.TestConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestConfig.class})
public class WordCounterTest {
	
	@Autowired
	WordCounter wordCounter;
	
//	@Mock
//	ValidationHelper helper;
	
	@Before
	public void setup(){
		wordCounter.addWordToWordCounterList("Flower");
		wordCounter.addWordToWordCounterList("flower");
		wordCounter.addWordToWordCounterList("1234");
		wordCounter.addWordToWordCounterList("flor");
		wordCounter.addWordToWordCounterList("Flor");
		wordCounter.addWordToWordCounterList("coffee");
		wordCounter.addWordToWordCounterList("stone");
	}
	
	@Test
	public void testGetCountofWord() {
		int size = wordCounter.getWordCounterWordCount("flower"); 
		assertEquals(4, size);
		
		size = wordCounter.getWordCounterWordCount("stone"); 
		assertEquals(1, size);
		
		size = wordCounter.getWordCounterWordCount("flor"); 
		assertEquals(4, size);
		
	}
	
	
}

