package com.shan.validation;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import com.shan.config.TestConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestConfig.class})
public class ValidationHelperTest {
	
	@Autowired
	ValidationHelper validationHelper;
	
	@Test
	public void testWordValidationNull() {
		String word = null;
		List<String> errors = validationHelper.validate(word);
		assertTrue(!errors.isEmpty());
	}

	@Test
	public void testWordValidationNumber() {
		String word = "1234ABC";
		assertTrue(!validationHelper.validate(word).isEmpty());
	
	}
	
	@Test
	public void testWordValidationSpecialChars() {
		String word = "abcd_";
		assertTrue(!validationHelper.validate(word).isEmpty());
		
		String word2 = "�ABCD#";
		assertTrue(!validationHelper.validate(word2).isEmpty());
	
	}
	
	@Test
	public void testWordValidationAlphabets() {
		String word = "abcd";
		assertTrue(validationHelper.validate(word).isEmpty());
		
		String word2 = "ABCD";
		assertTrue(validationHelper.validate(word2).isEmpty());
		
		String word3 = "ABCDabcd";
		assertTrue(validationHelper.validate(word3).isEmpty());
	
	}
}
