package com.shan.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.shan.helper.TranslatorHelper;
import com.shan.translator.TranslatorFactory;
import com.shan.validation.ValidationHelper;
import com.shan.wordcounter.WordCounter;
import com.shan.wordcounter.WordCounterImpl;

@Configuration
public class TestConfig {
	
	@Bean
	ValidationHelper getValidationHelper() {
		return new ValidationHelper();
	}
	
	@Bean
	WordCounter getWordCounterImpl() {
		return new WordCounterImpl();
	}

	@Bean
	TranslatorHelper getTranslatorHelper() {
		return new TranslatorHelper();
	}
	
	@Bean
	TranslatorFactory getTranslatorFactory() {
		return new TranslatorFactory();
	}
}
